#ifndef TRANSMITTER_DEFINED
#define TRANSMITTER_DEFINED
#include <zephyr.h>
#include <stdint.h>
#include <bluetooth/bluetooth.h>

class Transmitter
{
public:
    #define MANUFACTURER_DATA_LEN 4
    #define ADVDATACNT 2
    static void initHW(uint16_t mID, uint8_t grp);
    static void send(uint8_t value);
private:
    static uint8_t byte;
    static uint16_t mfgID;
    static uint8_t group;
    static struct bt_le_adv_param advParam;
    static uint8_t mfgData[4];
    static struct bt_data advData[ADVDATACNT];
    static uint8_t advFlags;
};
#endif