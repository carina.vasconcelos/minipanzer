#ifndef RECEIVER_DEFINED
#define RECEIVER_DEFINED

#include <zephyr.h>
#include <bluetooth/bluetooth.h>

class Receiver
{
public:
    #define MANUFACTURER_DATA_LEN 4
    static void initHW(uint16_t mID, uint8_t grp);
    static void listen();
    static uint8_t receive();
private: 
    static uint8_t mfgIDL;
    static uint8_t mfgIDH;
    static uint8_t group;
    static uint8_t cmd;
    static struct bt_le_scan_param scanParam;   
    //private data parsing method
    static bool dataParser(struct bt_data *data, void *userData);
    //private receive handler
    static void onReceive(const bt_addr_le_t *addr, 
                          int8_t rssi, 
                          uint8_t advType, 
                          struct net_buf_simple *buf);

};
#endif