#include "xf/xf.h"
#include "button/button.h"
#include "motor/motor.h"
#include "radio/transmitter.h"
#include "radio/receiver.h"
//#include "../ledmatrix/ledmatrix.h"

#ifndef FACTORY_ONCE
#define FACTORY_ONCE

#define TRANSMITTER (CONFIG_BT_PERIPHERAL == 1)
#define RECEIVER (CONFIG_BT_CENTRAL == 1)
#define COSTUM_CONTROLLER 1

class Factory
{
public:
    static void init();
    static void build();
    static void start();
    #if (TRANSMITTER != 0)
    #if (COSTUM_CONTROLLER == 0)
    static Button* btnA() {return &_btnA;}
    static Button* btnB() {return &_btnB;}
    #endif
    // Costum Controller
    #if (COSTUM_CONTROLLER != 0)
    static Button* LF() {return &_LF;}
    static Button* LB() {return &_LB;}
    static Button* RF() {return &_RF;}
    static Button* RB() {return &_RB;}
    #endif
    #endif
    #if (RECEIVER != 0)
    static Motor* mA() {return &_mA;}
    static Motor* mB() {return &_mB;}
    #endif
    //static LEDMatrix* m() {return &_m;}
    static XF* xf() {return XF::getInstance();}


   
private:
    Factory(/* args */);
    ~Factory();

    #if (TRANSMITTER != 0)
    #if (COSTUM_CONTROLLER == 0)
    static Button _btnA;
    static Button _btnB;
    #endif
    // Costum Controller
    #if (COSTUM_CONTROLLER != 0)
    static Button _LF;
    static Button _LB;
    static Button _RF;
    static Button _RB;  
    #endif
    #endif
    #if (RECEIVER != 0)
    static Motor _mA;
    static Motor _mB;
    #endif
    //static LEDMatrix _m;
};

#endif