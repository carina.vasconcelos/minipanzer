#ifndef MATRIX_ONCE
#define MATRIX_ONCE
#include <stdint.h>
#include <zephyr.h>
#include <display/mb_display.h>

class LEDMatrix
{
public:
    #define ROWS 5
    #define COLS 5
    LEDMatrix();
    ~LEDMatrix();
    void initHW();
    void on(uint8_t r,int c);
    void off(uint8_t r,int c);
    void clear();
    void blind();
    void test();
    void displayNumber(int num);
    void displayText(const char* text);
private: 
    uint8_t leds[ROWS][COLS];
    struct mb_display*  disp;
};
#endif