#include "motor/motor.h"

Motor::Motor()
{

}
Motor::~Motor()
{
}
void Motor::initHW(uint8_t p1, const char* port1,
              uint8_t p2, const char* port2)
{
    pin1 = p1;
    pin2 = p2;
    driver1 = const_cast<device*> (device_get_binding(port1));
    driver2 = const_cast<device*> (device_get_binding(port2));
    printk("driver of pin %d is %d\n", pin1, driver1);    
    printk("driver of pin %d is %d\n", pin2, driver2); 
    int ret = gpio_pin_configure(driver1,pin1,GPIO_OUTPUT | GPIO_OUTPUT_INIT_LOW);
    printk("pin %d configuration state is %d\n", pin1, ret);
    ret = gpio_pin_configure(driver2,pin2,GPIO_OUTPUT | GPIO_OUTPUT_INIT_LOW);
    printk("pin %d configuration state is %d\n", pin2, ret);      
}
void Motor::forward()
{
    //stop();
    //k_sleep(K_MSEC(10));
    gpio_pin_set(driver1,pin1,1);
    gpio_pin_set(driver2,pin2,0);
}
void Motor::backward()
{
    //stop();
    //k_sleep(K_MSEC(10));
    gpio_pin_set(driver1,pin1,0);
    gpio_pin_set(driver2,pin2,1);
}
void Motor::stop()
{
    gpio_pin_set(driver1,pin1,0);
    gpio_pin_set(driver2,pin2,0);
}