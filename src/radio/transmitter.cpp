#include "radio/transmitter.h"

#include <bluetooth/bluetooth.h>

uint8_t Transmitter::byte = -1;
uint16_t Transmitter::mfgID = 0x0;
uint8_t Transmitter::group = 0;
struct bt_le_adv_param Transmitter::advParam {};
uint8_t Transmitter::mfgData[4] = {};
struct bt_data Transmitter::advData[ADVDATACNT];
uint8_t Transmitter::advFlags;

/*
Transmitter::Transmitter()
{
    mfgID = 0x0;
    group = 0;
    mfgData[0]=0;
    mfgData[1]=0;
    mfgData[2]=0;
    mfgData[3]=0;
    byte = -1;
    advFlags = 0;
}
Transmitter::~Transmitter()
{

}
*/
void Transmitter::initHW(uint16_t mID, uint8_t grp)
{
    mfgID = mID;
    group = grp;

    mfgData[0]=mfgID;
    mfgData[1]=mfgID  >> 8;
    mfgData[2]=group;
    mfgData[3]=0;

    advParam.id = BT_ID_DEFAULT;
	advParam.sid = 0;
	advParam.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
	advParam.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
	advParam.options = BT_LE_ADV_OPT_USE_NAME;
	advParam.peer = NULL;
	advParam.secondary_max_skip = 0;

    advFlags = BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR;

    advData[0].type = BT_DATA_FLAGS;
    advData[0].data = &advFlags;
    advData[0].data_len = 1;

    advData[1].type = BT_DATA_MANUFACTURER_DATA;
    advData[1].data = mfgData;
    advData[1].data_len = 4;

    int btErr = bt_enable(NULL);
    printk("BT enable error = %d",btErr);
}

void Transmitter::send(uint8_t value)
{
    if (byte != value)
    {
        bt_le_adv_stop();
        byte = value;
        mfgData[3]=byte;
        int advErr = bt_le_adv_start(&advParam,advData,ARRAY_SIZE(advData),NULL,0);
        printk("advertise status %d\n",advErr);
    }
}
