#include "factory/factory.h"

#if (TRANSMITTER != 0) 
#if (COSTUM_CONTROLLER == 0)
Button Factory::_btnA;
Button Factory::_btnB;
#endif
#if (COSTUM_CONTROLLER != 0)
Button Factory::_LF;
Button Factory::_LB;
Button Factory::_RF;
Button Factory::_RB;
//Transmitter Factory::_tx;
#endif
#endif
#if (RECEIVER != 0)
Motor Factory::_mA;
Motor Factory::_mB;
#endif
//LEDMatrix Factory::_m;

Factory::Factory()
{
}

Factory::~Factory()
{
}

void Factory::init() 
{
    #if (TRANSMITTER != 0)
    #if (COSTUM_CONTROLLER == 0)
    btnA()->initHW(17,"GPIO_0");
    btnB()->initHW(26,"GPIO_0");
    #endif
    #if (COSTUM_CONTROLLER != 0)
    LF()->initHW(23,"GPIO_0");
    LB()->initHW(22,"GPIO_0");
    RF()->initHW(16,"GPIO_0");
    RB()->initHW(21,"GPIO_0");
    Transmitter::initHW(0x025a,3);
    #endif
    #endif
    #if (RECEIVER != 0)
    mA()->initHW(16,"GPIO_0",21,"GPIO_0");
    mB()->initHW(23,"GPIO_0",22,"GPIO_0");
    Receiver::initHW(0x025a,3);
    #endif

    //m()->initHW();
    xf()->init();
}

void Factory::build()
{
}

void Factory::start()
{
    #if (TRANSMITTER != 0)
    #if (COSTUM_CONTROLLER == 0)
    btnA()->startBehaviour();
    btnB()->startBehaviour();
    #endif
    #if (COSTUM_CONTROLLER != 0)
    LF()->startBehaviour();
    LB()->startBehaviour();
    RF()->startBehaviour();
    RB()->startBehaviour();
    #endif
    #endif
}

