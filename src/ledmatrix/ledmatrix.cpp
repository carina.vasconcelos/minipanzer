#include "ledmatrix/ledmatrix.h"

LEDMatrix::LEDMatrix()
{
    for (int r=0; r<ROWS;r++)
    {
        for (int c=0; c<COLS;c++)
        {
            leds[r][c]=0;
        }
    }
}

LEDMatrix::~LEDMatrix()
{

}

void LEDMatrix::initHW()
{
   disp = mb_display_get();
   printk("%d",disp);
}

void LEDMatrix::on(uint8_t r,int c)
{
    leds[r][c] = 1;
    struct mb_image pixels;
    for (int rw=0; rw<ROWS;rw++)
    {
        pixels.r[rw].c1 = leds[rw][0]; 
        pixels.r[rw].c2 = leds[rw][1]; 
        pixels.r[rw].c3 = leds[rw][2]; 
        pixels.r[rw].c4 = leds[rw][3]; 
        pixels.r[rw].c5 = leds[rw][4]; 
    }
    mb_display_image(disp,MB_DISPLAY_MODE_DEFAULT,SYS_FOREVER_MS,&pixels,1);
}

void LEDMatrix::off(uint8_t r,int c)
{
    leds[r][c] = 0;
    struct mb_image pixels;
    for (int rw=0; rw<ROWS;rw++)
    {
        pixels.row[rw]=0;
        for (int cl=0; cl<COLS;cl++)
        {
            pixels.row[rw] |= BIT(leds[rw][cl]);
        } 
    }
    mb_display_image(disp,MB_DISPLAY_MODE_DEFAULT,SYS_FOREVER_MS,&pixels,1);
}

void LEDMatrix::clear()
{
    for (int r=0; r<ROWS;r++)
    {
        for (int c=0; c<COLS;c++)
        {
            leds[r][c]=0;
        }
    }
    struct mb_image pixels = {};
    mb_display_image(disp,MB_DISPLAY_MODE_DEFAULT,SYS_FOREVER_MS,&pixels,1);
}

void LEDMatrix::blind()
{
    struct mb_image pixels;
    for (int rw=0; rw<ROWS;rw++)
    {
        pixels.row[rw]=0xFF; 
    }
    mb_display_image(disp,MB_DISPLAY_MODE_DEFAULT,SYS_FOREVER_MS,&pixels,1);
}

void LEDMatrix::test()
{
    for (int r=0; r<ROWS;r++)
    {
        for (int c=0; c<COLS;c++)
        {
            on(r,c);
            k_sleep(K_MSEC(500));
        }
    }
    for (int r=0; r<ROWS;r++)
    {
        for (int c=0; c<COLS;c++)
        {
            off(r,c);
            k_sleep(K_MSEC(500));
        }
    }
}

void LEDMatrix::displayNumber(int num)
{
    mb_display_print(disp,MB_DISPLAY_MODE_DEFAULT,SYS_FOREVER_MS,"%d",num);
}

void LEDMatrix::displayText(const char* text)
{
    mb_display_print(disp,MB_DISPLAY_MODE_DEFAULT,SYS_FOREVER_MS,"%s",text);
}