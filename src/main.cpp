#include <zephyr.h>
#include "factory/factory.h"
#include "button/button.h"

typedef union TRXBYTE
{
    struct 
    {
        unsigned b1 : 1;
        unsigned b2 : 1;
        unsigned b3 : 1;
        unsigned b4 : 1;
        unsigned b5 : 1;
        unsigned b6 : 1;
        unsigned b7 : 1;
        unsigned b8 : 1;
    } bits;
    uint8_t byte;
} TRXBYTE;

int main()
{
    Factory::init();
    Factory::build();
    Factory::start();

    #if (RECEIVER != 0)
    Receiver::listen();
    #endif

    TRXBYTE b;
    b.byte = 0;
    
    while(1)
    {
        
        
        #if (TRANSMITTER != 0)
        k_sleep(K_MSEC(20));

        #if (COSTUM_CONTROLLER == 0)
        if (Factory::btnA()->pressed())
        {
            printk("Button A pressed\n");
            b.bits.b1 = 1;
        }
        else
        {
            b.bits.b1 = 0;
        }
        if (Factory::btnB()->pressed())
        {
            printk("Button B pressed\n");
            b.bits.b2 = 1;
        }
        else
        {
            b.bits.b2 = 0;        
        }
        #endif

        #if (COSTUM_CONTROLLER != 0)
        if (Factory::LF()->pressed())
        {
            printk("Left Forward pressed\n");
            b.bits.b1 = 1;
        }
        else
        {
            b.bits.b1 = 0;
        }
        
        if (Factory::RF()->pressed())
        {
            printk("Right Forward pressed\n");
            b.bits.b2 = 1;
        }
        else
        {
            b.bits.b2 = 0;        
        }

        if (Factory::LB()->pressed())
        {
            printk("Left Backwards pressed\n");
            b.bits.b3 = 1;
        }
        else
        {
            b.bits.b3 = 0;
        }

        if (Factory::RB()->pressed())
        {
            printk("Right Backwards pressed\n");
            b.bits.b4 = 1;
        }
        else
        {
            b.bits.b4 = 0;
        }
        #endif

        Transmitter::send(b.byte);
        Factory::xf()->execute();
        #endif

        #if (RECEIVER != 0)
        k_sleep(K_MSEC(20));
        b.byte = Receiver::receive();
        if (b.bits.b1 == 1)
        {
            printk("Forward Left\n");
            Factory::mA()->forward();
        }
        else
        {
            Factory::mA()->stop();
        }
        if (b.bits.b2 == 1)
        {
            printk("Forward Right\n");
            Factory::mB()->forward();
        }
        else
        {
            Factory::mB()->stop();
        }
        if (b.bits.b3 == 1)
        {
            printk("Backward Left\n");
            Factory::mA()->backward();
        }
        if (b.bits.b4 == 1)
        {
            printk("Backward Right\n");
            Factory::mB()->backward();
        }
        
        #endif
    }
    return 0;
}
