Hey Devs, 

1. If you're writing a new feature/piece for our project, please add a new Branch from "Feature". Use the name of the feature as branch name and not your own name.
2. The documentation may and should be written here on gitlab, if needed a new copy will be made in word as soon as the project is done. 
3. Please document your functions properly so we know what each piece of code does. Try to use doxygen semantic so we can generate a doc in the end.
4. Header files (.h files) are now saved in the designated "include" folder. For some explanation you can check the main branch. 

Please follow following rules so we can have a clean and easy to manage repo.
Thanks for reading ^^
